<?php

namespace App\Migration;

use Spiral\Migrations\Migration;

class OrmDefault470b80ff6b2aa10aae16a624c2f8c720 extends Migration
{
    protected const DATABASE = 'default';

    public function up(): void
    {
        $this->database()->execute('ALTER TABLE user CONVERT TO CHARACTER SET utf8mb4;');
    }

    public function down(): void
    {
    }
}
