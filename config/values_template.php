<?php
// copy this file as values.php and fill in your values

declare(strict_types=1);

return [
    'db' => [
        'host' => 'localhost',
        'name' => '',
        'user' => '',
        'password' => '',
    ],
    'telegram' => [
        'key' => '',
        'webhookUrl' => '',
    ],
];
