<?php

declare(strict_types=1);

use App\Controller\SiteController;
use App\Controller\ApiController;
use Yiisoft\Router\Route;
use App\Service\ConfigValues;

return [
    Route::get('/')->action([SiteController::class, 'index'])->name('home'),
    Route::post('/' . ConfigValues::get('telegram.key'))->action([ApiController::class, 'webhook'])->disableMiddleware(Yiisoft\Csrf\CsrfMiddleware::class)->name('webhook'),
];
