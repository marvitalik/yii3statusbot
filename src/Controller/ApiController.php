<?php

declare(strict_types=1);

namespace App\Controller;

use Psr\Http\Message\ResponseInterface;
use Yiisoft\DataResponse\DataResponseFactory;
use Yiisoft\DataResponse\Formatter\JsonDataResponseFormatter;
use Psr\Http\Message\ServerRequestInterface;
use App\Service\Bot;


class ApiController
{
    private DataResponseFactory $dataResponseFactory;
    private JsonDataResponseFormatter $formatter;
    private Bot $bot;

    public function __construct(DataResponseFactory $dataResponseFactory, JsonDataResponseFormatter $formatter, Bot $bot)
    {
        $this->dataResponseFactory = $dataResponseFactory;
        $this->formatter = $formatter;
        $this->bot = $bot;
    }

    public function webhook(ServerRequestInterface $request): ResponseInterface
    {
        $body = \Safe\json_decode($request->getBody()->getContents()); // TODO: move to middleware in route definition and format to JSON
        $result = $this->bot->processWebhook($body);
        $response = $this->dataResponseFactory->createResponse($result);

        return $response->withResponseFormatter($this->formatter);
    }
}
