<?php

declare(strict_types=1);

namespace App\Service;

use Psr\Http\Client\ClientInterface;
use Yiisoft\Http\Status;

final class Telegram
{
    private const API_URL = 'https://api.telegram.org/';
    
    private const METHOD_GET_UPDATES = 'getUpdates';
    
    private const METHOD_SEND_MESSAGE = 'sendMessage';
    
    private const METHOD_WEBHOOK_INFO = 'getWebhookInfo';
    
    private const METHOD_WEBHOOK_SET = 'setWebhook';
    
    private const METHOD_WEBHOOK_DELETE = 'deleteWebhook';
    
    private ClientInterface $client;
    
    private string $apiKey;
    
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
        $this->apiKey = ConfigValues::get('telegram.key');
    }
    
    private function request(string $method, ?array $payload = null): \stdClass
    {
        if ($payload) {
            $response = $this->client->post(self::API_URL . 'bot' . $this->apiKey . '/' . $method, [
                'json' => $payload,
            ]);
        } else {
            $response = $this->client->get(self::API_URL . 'bot' . $this->apiKey . '/' . $method);
        }

        if ($response->getStatusCode() == Status::OK) {
            return \Safe\json_decode($response->getBody()->getContents());
        }
        return new \stdClass(); // TODO: make processing incorrect responses
    }
    
    public function getUpdates(): array
    {
        $webhook = $this->request(self::METHOD_GET_UPDATES);
        if (isset($webhook->ok) && $webhook->ok == 1) {
            return $webhook->result;
        }
        return [];
    }
    
    public function sendMessage(int $chatId, string $message): void
    {
        $this->request(self::METHOD_SEND_MESSAGE, [
            'chat_id' => $chatId,
            'text'    => $message,
        ]);
    }
    
    public function webhookInfo(): \stdClass
    {
        return $this->request(self::METHOD_WEBHOOK_INFO);
    }
    
    public function setWebhook(): \stdClass
    {
        return $this->request(self::METHOD_WEBHOOK_SET, [
            'url' => ConfigValues::get('telegram.webhookUrl') . $this->apiKey,
        ]);
    }
    
    public function deleteWebhook(): \stdClass
    {
        return $this->request(self::METHOD_WEBHOOK_DELETE);
    }
}
