<?php

declare(strict_types=1);

namespace App\Service;

final class ConfigValues
{
    public static function get(string $key)
    {
        $values = require __DIR__ . '/../../config/values.php';
        foreach (explode('.', $key) as $item) {
            if (isset($values[$item])) {
                $values = $values[$item];
            }
        }
        return $values;
    }
}
