<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\ReleaseStatus;
use App\Entity\User;
use Psr\Http\Client\ClientInterface;
use Yiisoft\Http\Status;
use Cycle\ORM\ORMInterface;
use Cycle\ORM\Transaction;
use App\Service\Telegram;

final class Bot
{
    private ClientInterface $client;
    
    private ORMInterface $orm;
    
    private Telegram $telegram;
    
    public function __construct(ClientInterface $client, ORMInterface $orm, Telegram $telegram)
    {
        $this->client = $client;
        $this->orm = $orm;
        $this->telegram = $telegram;
    }

    public function checkReleaseStatus(): void
    {
        $releaseStatus = $this->orm->getRepository(ReleaseStatus::class)->findOne();
        /** @var ReleaseStatus $releaseStatus */
        if (!$releaseStatus) {
            $releaseStatus = new ReleaseStatus();
        }

        $response = $this->client->get(ReleaseStatus::CHECK_URL);
        if ($response->getStatusCode() == Status::OK) {
            $content = $response->getBody()->getContents();
            $matches = [];
            preg_match('/\<h2\>Released \<b\>(?<released>\d+)\/(?<total>\d+)\<\/b\> packages\<\/h2\>/', $content, $matches);
            
            if (isset($matches['released']) && isset($matches['total'])) {
                $valReleased = (int) $matches['released'];
                $valTotal = (int) $matches['total'];
                
                if ($valReleased !== $releaseStatus->getReleasedPackages() || $valTotal !== $releaseStatus->getTotalPackages()) {
                    $releaseStatus->setAttributes([
                        'releasedPackages' => $valReleased,
                        'totalPackages'    => $valTotal,
                    ]);
                    $releaseStatus->touchChangedAt();

                    $this->notifyReleaseStatus($releaseStatus);
                }
                $releaseStatus->touchCheckedAt();
                $this->saveEntity($releaseStatus);
            }
        }
    }
    
    public function notifyReleaseStatus(?ReleaseStatus $releaseStatus = null, ?array $users = null, bool $force = false): void
    {
        if (!$releaseStatus) {
            $releaseStatus = $this->orm->getRepository(ReleaseStatus::class)->findOne();
        }
        
        $total = $releaseStatus->getTotalPackages();
        $released = $releaseStatus->getReleasedPackages();
        $message = 'Total packages: ' . $total . "\n" . 
            'Released packages: ' . $released . "\n" . 
            'Release complete: ' . round($released * 100 / $total, 1) . "%\n" . 
            'Last changed: ' . $releaseStatus->getChangedAt()->format('Y-m-d H:i') . ' (UTC)';
        
        if (is_null($users)) {
            $users = $this->orm->getRepository(User::class)->findAll();
        }
        
        foreach ($users as $user) {
            if ($user->getIsSubscribed() || $force) {
                $this->telegram->sendMessage($user->getChatId(), $message);
            }
        }
    }
    
    public function readWebhooks(): ?array
    {
        $webhooks = $this->telegram->getUpdates();
        if ($webhooks) {
            $update = array_pop($webhooks);
            $this->processWebhook($update);
            
            return (array) $update;
        }

        return null;
    }
    
    public function processWebhook(\stdClass $data): bool
    {
        if (isset($data->message)) {
            $chat = $data->message->chat;
            $user = $this->orm->getRepository(User::class)->findOne(['chat_id' => $chat->id]);

            if (!$user) {
                $user = new User();

                $user->setAttributes([
                    'username'  => $chat->username ?? null,
                    'firstName' => $chat->first_name ?? null,
                    'lastName'  => $chat->last_name ?? null,
                    'chatId'    => $chat->id,
                ]);
            }

            if (isset($data->message->text)) {
                $this->makeAnswer($user, $data->message->text);
            }
            
            $this->saveEntity($user);
            return true;
        }
        return false;
    }
    
    private function makeAnswer(User $user, string $message): void
    {
        switch (strtolower($message)) {
            case '/show':
                $this->notifyReleaseStatus(null, [$user], true);
                break;
            case '/on':
                $user->setSubscribed(true);
                $this->telegram->sendMessage($user->getChatId(), 'You are subscribed to notifications about changes count of relesed packages');
                break;
            case '/off':
                $user->setSubscribed(false);
                $this->telegram->sendMessage($user->getChatId(), 'You are unsubscribed from notifications about changes count of relesed packages');
                break;
            case '/status':
                $this->telegram->sendMessage($user->getChatId(), 'You are ' . ($user->getIsSubscribed() ? 'subscribed to' : 'unsubscribed from') . ' notifications about changes count of relesed packages');
                break;
            case '/start':
                $this->telegram->sendMessage($user->getChatId(), 'Type "/" for get avaiable commands for this bot');
                break;
            default:
                // no action
        }
    }

    private function saveEntity($entity): void // TODO: Make common class for entities (BaseEntity) and extend entities from it, move this method there, use it instead of OrmTrait
    {
        $tr = new Transaction($this->orm);
        $tr->persist($entity);
        $tr->run();
    }
}
