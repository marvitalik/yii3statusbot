<?php

declare(strict_types=1);

namespace App\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use App\Service\Bot;
use App\Service\Telegram;
use Yiisoft\Yii\Console\ExitCode;


final class ManageWebhooks extends \Symfony\Component\Console\Command\Command
{
    private const COMMAND_READ = 'read';
    
    private const COMMAND_SET = 'set';
    
    private const COMMAND_DELETE = 'delete';
    
    private const COMMAND_STATUS = 'status';
    
    private Bot $bot;
    
    private Telegram $telegram;

    public function __construct(Bot $bot, Telegram $telegram)
    {
        parent::__construct();
        $this->bot = $bot;
        $this->telegram = $telegram;
    }

    protected function configure()
    {
        $this->addArgument('wCommand', InputArgument::REQUIRED, 'Webhook command');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        switch ($input->getArgument('wCommand')) {
            case self::COMMAND_STATUS:
                return $this->webhookInfo();
            case self::COMMAND_SET:
                return $this->setWebhook();
            case self::COMMAND_DELETE:
                return $this->deleteWebhook();
            case self::COMMAND_READ:
                return $this->readWebhooks();
        }
        return ExitCode::NOINPUT;
    }
    
    private function readWebhooks(): int
    {
        try {
            $result = $this->bot->readWebhooks();
            if ($result) {
                print_r($result);
            }
        } catch (\Throwable $ex) {
            echo 'Some error occured!' . PHP_EOL;
            echo $ex->getMessage() . PHP_EOL;

            return ExitCode::UNSPECIFIED_ERROR;
        }

        return ExitCode::OK;
    }
    
    private function webhookInfo(): int
    {
        print_r((array) $this->telegram->webhookInfo());
        return ExitCode::OK;
    }
    
    private function setWebhook(): int
    {
        $response = $this->telegram->setWebhook();
        if (isset($response->description)) {
            echo $response->description . PHP_EOL;
            return ExitCode::OK;
        }

        echo 'FAIL' . PHP_EOL;
        return ExitCode::UNSPECIFIED_ERROR;
    }
    
    private function deleteWebhook(): int
    {
        $response = $this->telegram->deleteWebhook();
        if (isset($response->description)) {
            echo $response->description . PHP_EOL;
            return ExitCode::OK;
        }

        echo 'FAIL' . PHP_EOL;
        return ExitCode::UNSPECIFIED_ERROR;
    }

}
