<?php

declare(strict_types=1);

namespace App\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\Bot;
use Yiisoft\Yii\Console\ExitCode;


final class CheckStatus extends \Symfony\Component\Console\Command\Command
{
    private Bot $bot;

    public function __construct(Bot $bot)
    {
        parent::__construct();
        $this->bot = $bot;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $this->bot->checkReleaseStatus();
        } catch (\Throwable $ex) {
            echo 'Some error occured!' . PHP_EOL;
            echo $ex->getMessage() . PHP_EOL;

            return ExitCode::UNSPECIFIED_ERROR;
        }

        return ExitCode::OK;
    }
}
