<?php

declare(strict_types=1);

namespace App\Entity;

trait OrmTrait
{
    public function setAttributes(array $attributes): void
    {
        foreach ($attributes as $property => $value) {
            if (property_exists($this, $property)) {
                $this->$property = $value;
            }
        }
    }
}
