<?php

declare(strict_types=1);

namespace App\Entity;

use Cycle\Annotated\Annotation\Column;
use Cycle\Annotated\Annotation\Entity;

/**
 * @Entity(table="release_status")
 */
class ReleaseStatus
{
    use OrmTrait;

    public const CHECK_URL = 'https://www.yiiframework.com/yii3-progress';

    /**
     * @Column(type="primary")
     */
    private ?int $id = null;

    /**
     * @Column(type="integer")
     */
    private int $releasedPackages = 0;

    /**
     * @Column(type="integer")
     */
    private int $totalPackages = 0;

    /**
     * @Column(type="integer")
     */
    private int $checkedAt;
    
    /**
     * @Column(type="integer")
     */
    private int $changedAt;
    
    public function getReleasedPackages(): int
    {
        return $this->releasedPackages;
    }
    
    public function getTotalPackages(): int
    {
        return $this->totalPackages;
    }
    
    public function getCheckedAt(): \DateTime
    {
        return new \DateTime('@' . $this->checkedAt);
    }
    
    public function getChangedAt(): \DateTime
    {
        return new \DateTime('@' . $this->changedAt);
    }
    
    public function touchCheckedAt(): void
    {
        $this->checkedAt = time();
    }
    
    public function touchChangedAt(): void
    {
        $this->changedAt = time();
    }
}
