<?php

declare(strict_types=1);

namespace App\Entity;

use Cycle\Annotated\Annotation\Column;
use Cycle\Annotated\Annotation\Entity;
use Cycle\Annotated\Annotation\Table;
use Cycle\Annotated\Annotation\Table\Index;

/**
 * @Entity(table="user")
 * @Table(
 *      indexes={
 *          @Index(columns={"chat_id"}, unique=true)
 *      }
 * )
 */
class User
{
    use OrmTrait;

    /**
     * @Column(type="primary")
     */
    private ?int $id = null;

    /**
     * @Column(type="string(255)", nullable=true)
     */
    private ?string $username = null;

    /**
     * @Column(type="string(255)", nullable=true)
     */
    private ?string $firstName = null;

    /**
     * @Column(type="string(255)", nullable=true)
     */
    private ?string $lastName = null;

    /**
     * @Column(type="integer(11)")
     */
    private int $chatId;

    /**
     * @Column(type="boolean", default=true)
     */
    private bool $isSubscribed = true;
    
    public function getIsSubscribed(): bool
    {
        return $this->isSubscribed;
    }

    public function setSubscribed(bool $isSubscribed): void
    {
        $this->isSubscribed = $isSubscribed;
    }

    public function getChatId(): int
    {
        return $this->chatId;
    }
}
